import React, { Component } from 'react';
import './App.css';
import constant from './constants';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

class App extends Component {
  constructor() {
    super();
    this.state = {
      api1Response: "",
      api2Response: "",
      api3Response: "",
      api4Response: "",

    }
  }

  api1Handler = async (event) => {
    event.preventDefault();
    var tEmail = event.target.teacherEmail.value;
    var sEmail = event.target.studentEmail.value.split(",");
    await fetch(constant.apiHost + '/api/register', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "teacher": tEmail,
        "students": sEmail
      })
    }).then(async (res, data) => {
      var errorMsg = "";
      if (res.status == 422) {
        await res.json().then(function (object) {
          errorMsg = object.message;
        })
      }
      this.setState({ api1Response: res.status + " " + res.statusText, api1Error: errorMsg })
    }).catch(function (err) { console.log(err.type, err.message) });
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  api2Handler = async (event) => {
    event.preventDefault();
    var tEmail = this.state.api2TeacherEmail != undefined ? this.state.api2TeacherEmail : "";
    var requestString = '/api/commonstudents?';
    tEmail = tEmail.split(",");
    for (var i = 0; i < tEmail.length; i++) {
      requestString += 'teacher=' + tEmail[i].trim() + (i + 1 < tEmail.length ? '&' : '');
    }
    console.log(requestString)
    await fetch(constant.apiHost + requestString, {
      method: 'get',
      headers: { 'Content-Type': 'application/json' }
    }).then(async (res, data) => {
      var responseMsg = "";
      await res.json().then(function (object) {
        responseMsg = JSON.stringify(object);
      })
      console.log(responseMsg.toString())
      this.setState({ api2Response: res.status + " " + res.statusText, responseMsgApi2: responseMsg })
    }).catch(function (err) { console.log(err.type, err.message) });
  }


  api3Handler = async (event) => {
    event.preventDefault();
    var sEmail = this.state.suspendSemail != undefined ? this.state.suspendSemail : "";
    await fetch(constant.apiHost + '/api/suspend', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "student": sEmail
      })
    }).then(async (res, data) => {
      var errorMsg = "";
      if (res.status == 422) {
        await res.json().then(function (object) {
          errorMsg = object.message;
        })
      }
      this.setState({ api3Response: res.status + " " + res.statusText, api3Error: errorMsg })
    }).catch(function (err) { console.log(err.type, err.message) });
  }


  api4Handler = async (event) => {
    event.preventDefault();
    var tEmail = this.state.sender != undefined ? this.state.sender : "";
    var notification = this.state.notification != undefined ? this.state.notification : "";
    console.log(JSON.stringify({
      "teacher": tEmail,
      "notification": notification
    }))
    await fetch(constant.apiHost + '/api/retrievefornotifications', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "teacher": tEmail,
        "notification": notification
      })
    }).then(async (res, data) => {
      var responseMsg = "";
      if (res.status == 404) {
        await res.json().then(function (object) {
          responseMsg = object.message;
        })
      }
      if (res.status == 200) {
        await res.json().then(function (object) {
          responseMsg = JSON.stringify(object);
        })
      }
      console.log(responseMsg)
      this.setState({ api4Response: res.status + " " + res.statusText, responseMsg: responseMsg })
    }).catch(function (err) { console.log(err.type, err.message) });
  }


  render() {
    return (
      <Container>
        <div style={{ padding: 16 }}>
          {/* api 1 */}
          <p>1. Register Teacher and one or more students</p>
          <Form onSubmit={this.api1Handler}>
            <Form.Group>
              <Form.Label>Teacher Email</Form.Label>
              <Form.Control name="teacherEmail" type="email" placeholder="Teacher email" required />
            </Form.Group>

            <Form.Group>
              <Form.Label>Student Email</Form.Label>
              <Form.Control name="studentEmail" type="text" placeholder="Student email" required />
              <Form.Text className="text-muted">
                Use comma to seperate email if you want to enter multiple email.
            </Form.Text>
            </Form.Group>
            <Button variant="primary" type="submit">
              Test API
        </Button>
          </Form>
          <Form.Text className="text-muted">{this.state.api1Response ? "response code : " + this.state.api1Response : ""}</Form.Text>
          <Form.Text className="text-muted">{this.state.api1Error ? "response message : " + this.state.api1Error : ""}</Form.Text>
          <hr />


          {/* api 2 */}

          <p>2. Retrieve a list of students common to a given list of teachers</p>
          <Form>
            <Form.Group >
              <Form.Label>Teacher Email</Form.Label>
              <Form.Control name="api2TeacherEmail" type="email" placeholder="Teacher email" value={this.state.api2TeacherEmail} onChange={this.handleChange} />
              <Form.Text className="text-muted">
                Use comma to seperate email if you want to enter multiple email.
            </Form.Text>
            </Form.Group>
            <Button variant="primary" onClick={this.api2Handler} type="submit">
              Test API
        </Button>
          </Form>
          <Form.Text className="text-muted">{this.state.api2Response ? "response code : " + this.state.api2Response : ""}</Form.Text>
          <Form.Text className="text-muted">{this.state.responseMsgApi2 ? "response message : " + this.state.responseMsgApi2 : ""}</Form.Text>
          <hr />


          {/* api 3 */}

          <p>3. Suspend a specified student</p>
          <Form>
            <Form.Group onSubmit={this.api2Handler}>
              <Form.Label>Student Email</Form.Label>
              <Form.Control type="email" placeholder="Student email" required name="suspendSemail" onChange={this.handleChange} />
            </Form.Group>
            <Button variant="info" type="submit" onClick={this.api3Handler}>
              Test API
        </Button>
          </Form>
          <Form.Text className="text-muted">{this.state.api3Response ? "response code : " + this.state.api3Response : ""}</Form.Text>
          <Form.Text className="text-muted">{this.state.api3Error ? "response message : " + this.state.api3Error : ""}</Form.Text>
          <hr />



          {/* api 4 */}


          <p>4. Send a notification, showing the list of recipients</p>
          <Form>
            <Form.Group>
              <Form.Label>Teacher Email</Form.Label>
              <Form.Control type="email" name="sender" onChange={this.handleChange} placeholder="Teacher email" required />
            </Form.Group>
            <Form.Group>
              <Form.Label>Notification</Form.Label>
              <Form.Control type="text" name="notification" onChange={this.handleChange} placeholder="Notification message" required />
            </Form.Group>
            <Button variant="success" type="submit" onClick={this.api4Handler} >
              Test API
        </Button>
          </Form>
          <Form.Text className="text-muted">{this.state.api4Response ? "response code : " + this.state.api4Response : ""}</Form.Text>
          <Form.Text className="text-muted">{this.state.responseMsg ? "response message : " + this.state.responseMsg : ""}</Form.Text>


        </div>
      </Container>
    );
  }
}

export default App;
